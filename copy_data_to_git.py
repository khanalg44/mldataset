#!/usr/bin/env python3

import subprocess, os, sys, shutil

def CopyToDataDir(src_dir, dest_dir):
    base=os.path.basename(src_dir)
    dest_dir=os.path.join(dest_dir, base)
    shutil.copytree(src_dir, dest_dir)
    print ("copied the src_directory to the ml_dataset directory.")

def PushToGit():
    cmd='cd /Users/gshyam/projects/work_projects/machine_learning/ml_dataset'
    subprocess.call(cmd, shell=True)
    cmd="ls ./*"
    subprocess.call(cmd, shell=True)
    cmd="git add . "
    subprocess.call(cmd, shell=True)
    cmd="git commit -m ' added new dataset'"
    subprocess.call(cmd, shell=True)
    cmd="git push -u origin master"
    subprocess.call(cmd, shell=True)

    return 0



if __name__=="__main__":

    dest_dir='/Users/gshyam/projects/work_projects/machine_learning/ml_dataset'

    #src_dir='small_dataset_train_1000_test_500'
    if len(sys.argv)<2:
        print ("Please give the dataset directory.")
        sys.exit()
    src_dir=sys.argv[1]
   
    src_dir=os.path.join(os.getcwd(), src_dir)

    print ("src_dir:",src_dir)
    print ("dest_dir:", dest_dir)
    #1
    CopyToDataDir(src_dir, dest_dir)
    #2
    #PushToGit()

